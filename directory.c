#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "directory.h"

/* +--------------------------------------------+
 * | How a Directory is represented in a file:  |
 * |                                            |
 * | the main directory followed by a list of   |
 * | file_inode records:                        |
 * | {                                          |
 * | 	size,                                   |
 * |		uid,                                |
 * |		gid,                                |
 * |		mode,                               |
 * |		atime,                              |
 * |		ctime,                              |
 * |		mtime,                              |
 * |		linkcount                           |
 * |	}                                       |
 * | f:foo:1234                                 |
 * | d:.:10                                     |
 * | d:..:102                                   |
 * | f:bar:2245                                 |
 * |                                            |
 * | A function is needed to convert the        |
 * | Filename_Inode struct into a string:       |
 * | char* fitostr(Filename_Inode*)             |
 * | and vice-versa                             |
 * | Filename_Inode* strtofi(char*)             |
 * |                                            |
 * |                                            |
 * |                                            |
 * +--------------------------------------------+
 */



// DIRECTORY BUILDER //
void dir_build(Directory* d,int size, int uid, int gid, unsigned long mode, unsigned long atime, unsigned long ctime, unsigned long mtime, int linkcount)
{
	d->size = size;
	d->uid = uid;
	d->gid = gid;
	d->mode = mode;
	d->atime = atime;
	d->ctime = ctime;
	d->mtime = mtime;
	d->linkcount = linkcount;

}



// FI BUILDER //
void fi_build(Filename_Inode* fi, const char* type, const char* filename, int blocknum)
{
	char* new_type = malloc(2);
	char* new_filename = malloc(101);

	strcpy(new_type, type);
	strcpy(new_filename, filename);

	fi->type = new_type;
	fi->filename = new_filename;
	fi->blocknum = blocknum;

}

void fi_deconstruct(Filename_Inode* fi)
{
    free(fi->type);
    free(fi->filename);
}

void pr_deconstruct(parseReturn* p)
{
    free(p->parentName);
    free(p->childName);
}


// "Serialize"
void fitostr(Filename_Inode* fi, char* result)
{
	sprintf(result, "%s:%s:%d\n", fi->type, fi->filename, fi->blocknum);
}

// "Deserialize"
void strtofi(char* s, Filename_Inode* fi)
{
	char* str = malloc(200);
	sprintf(str, "%s", s);
	char* rest;

	char* type = strtok_r(str, ":", &rest);
	char* filename = strtok_r(NULL, ":", &rest);
	int block = atoi(strtok_r(NULL, ":", &rest));
    fi_build(fi, type, filename, block);
    // fi_build will make copies of the string
    free(str);
}

void dir_write(Directory* d, Filename_Inode* f_array, FILE* fp)
{
    fwrite(d, sizeof(Directory), 1, fp);

    int i;
    size_t f_array_len = d->linkcount;
    char fi_string[MAX_STRING_BUFFER];
    for(i = 0; i < f_array_len; i++)
    {
      fitostr(&f_array[i], fi_string);
      fputs(fi_string, fp);
    }
}

void dir_read(Directory* d, Filename_Inode** f_array, FILE* fp)
{
	// read in the directory
	fread(d, sizeof(Directory), 1, fp);
    // cursor is auto moved after reading in the directory

	// now create array based on linkcount:
	unsigned int linkcount = d->linkcount;
	// make the array 1 longer for adding new directories
	// this is still ok since writing loop is based on linkcount
	Filename_Inode* f_arrayp = calloc(linkcount+1, sizeof(Filename_Inode));
	//assert(f_array != NULL);

	int i;
	// read and deserialize:
	for(i = 0; i < linkcount; i++)
	{
        char fi_string[200] = "";// this is due to how strtok works
        fgets(fi_string, MAX_STRING_BUFFER, fp);
        strtofi(fi_string, &(f_arrayp[i]));
        // freeing the fi_string would destroy the filename inode because strtok just gives references
    }
    *f_array = f_arrayp;

}

void inode_write(Inode* i, FILE* fp)
{
    rewind(fp);
    fwrite(i, sizeof(Inode), 1, fp);
}

void inode_read(Inode* i, FILE* fp)
{
    rewind(fp);
    fread(i, sizeof(Inode), 1, fp);
}

void index_write(int* blocks, int count, FILE* fp)
{
    char out[4096] = "";
    int i;
/* save the blocks that are used:
 */
    for(i = 0; i < count; i++)
    {
        sprintf(out + strlen(out), "%d,%d,", i, blocks[i]);
    }
/* for the rest:
 * keep the same value of i as before
 */
    for(; i < 400; i++)
    {
        sprintf(out + strlen(out), "%d,%d,", i, -1);
    }
    sprintf(out+strlen(out)-1, "%s", "\0"); // final comma removal (aesthetics)
    rewind(fp);
    fwrite(out, 1, 4096, fp);
}

void index_read(int* blocks, FILE* fp)
{
    char in[4096] = "";
    char* rest = NULL;

    rewind(fp);
    fread(in, 1, sizeof(in), fp);

    strtok_r(in, ",", &rest);
    int i;
    for(i = 0; i < 400; i++)
    {
        blocks[i] = atoi(strtok_r(NULL, ",", &rest));
        strtok_r(NULL, ",", &rest);
    }
}

/*
int isUserOwner(int uid)
{
    return uid == geteuid();
}

int isUserInGroup(int gid)
{
    return gid == getegid();
}

*/

