#define FUSE_USE_VERSION 26

#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <time.h>

#include <unistd.h>
#include <sys/types.h>

#include "directory.h"

Superblock* SUPERBLOCK;
char* BLOCKPATHHEAD = "/fusedata/fusedata.";
int ROOTBLOCK;
char* ROOTBLOCKPATH;
unsigned int FREEBLOCKSCOUNT;
unsigned int INODECOUNT;

unsigned int DIRECTORYSIZE = 4096;

unsigned int DEFAULT_DIR_MODE = 775;

/* IMPLICITLY CONVERT TO DECIMAL FROM OCTAL
 * numbers get represented a binary in memory
 * numbers starting with 0 are converted to binary with the
 * perspective that they are octal
 *
 * THIS IS TO HANDLE PERMISSIONS CHECKING
 */

 unsigned int U_READ    = S_IRUSR;
 unsigned int U_WRITE   = S_IWUSR;
 unsigned int U_EXEC    = S_IXUSR;

 unsigned int G_READ    = S_IRGRP;
 unsigned int G_WRITE   = S_IWGRP;
 unsigned int G_EXEC    = S_IXGRP;

 unsigned int O_READ    = S_IROTH;
 unsigned int O_WRITE   = S_IWOTH;
 unsigned int O_EXEC    = S_IXOTH;

 unsigned int FILE_MODE = S_IFREG;
 unsigned int DIR_MODE  = S_IFDIR;

/* END CONVERSIONS */

int modeToOctal(unsigned int mode)
{
    char buffer[20] = "";
    sprintf(buffer, "%o", mode);
    int result = atoi(buffer);
    return result;
}



// will return a parentBlock = childBlock if new dir/file
int parsePath(const char* path, parseReturn* p)
{
/* Files reading */
    char filename[200] = "";
    sprintf(filename, "%s", ROOTBLOCKPATH);

    Directory* dir          = NULL; // just to be safe
    Filename_Inode* f_array = NULL;
    FILE* fp                = NULL;

    int notExist = 0;           // this is for when a file/directory cannot be found
    int wasFile = 0;            // if the previous iteration resulted in a file
                                // then we need to flag
    int wasLink = 0;
    int curBlock = ROOTBLOCK;   // we first read from the root directory
    int prevBlock = curBlock;   // curBlock is updated if the next thing can be found
                                // otherwise if curBlock != prevBlock then notExist

/* Parse Loop Set up*/
    char copypath[2000] = "";
    sprintf(copypath, "%s", path);
    char* rest;

    char* currentDir = "/";     // a copy of this will be made at the end if
                                // necessary (all strings are made into copies before
                                // returning
    char* previousDir = currentDir;
    char* token = strtok_r(copypath, "/", &rest);

/*
    "/abc/def/ghi"

    case 1: perfect directory structure:
    "abc", "def", "ghi"
    all exist

    case 2: last directory is missing:
    "abc", "def"
    exist but not "ghi"

    case 3: missing link:
    "abc"
    exist but not "def" and "ghi"
    ERROR

    "/abc/def/ghi.txt"

    case 4: perfect file structure:
    "abc", "def", "ghi.txt"
    all exist

    case 5: last file is missing:
    "abc", "def"
    exist but not "ghi.txt"

    case 6: file inbetween
    "/abc/def.txt/ghi"
    ERROR
*/

    int i;
    while(token != NULL) // there is a token
    {
    /* CASE 3: Double Failure
     * REQUIRES a previous iteration
     *
     * this scenario would occur if in the previous iteration
     * the file/directory that resulted did not exist
     * which means that if previous directory already did not exist
     * then why is there another token to find?
    */
        if(notExist == 1)
        {

            if(dir != NULL && f_array != NULL)  // free can work on NULL but dir
            {                                   // and f_array are accessed
                for(i = 0; i < dir->linkcount; i++)
                {
                    fi_deconstruct(&(f_array[i]));
                }
                free(f_array);
                free(dir);
            }
            return -ENOENT;
        }
    /* CASE 6: File Parent
     * REQUIRES a previous iteration
     *
     * this scenario would occur if in the previous iteration
     * a file was the result
     * how can a file be a parent?
    */
        if(wasFile == 1 || wasLink == 1)
        {

            if(dir != NULL && f_array != NULL)  // free can work on NULL but dir
            {                                   // and f_array are accessed
                for(i = 0; i < dir->linkcount; i++)
                {
                    fi_deconstruct(&(f_array[i]));
                }
                free(f_array);
                free(dir);
            }
            return -ENOTDIR;
        }
    /* CASE: Successful Parent Directory
     * REQUIRES a previous iteration
     *
     * The previous directory exists so attempt to locate current one
     * prep the dir and f_array as well as save curBlock in case it is
     * overwritten
     */
        if(dir != NULL && f_array != NULL)
        {
            for(i = 0; i < dir->linkcount; i++)
            {
                fi_deconstruct(&(f_array[i]));
            }
            free(f_array);
            free(dir);
        }

        prevBlock = curBlock;
    /* CASE:
     * DOES NOT REQUIRE a previous iteration
     *
     * begin parsing and searching
     */
     // open and read
        dir = calloc(1, sizeof(Directory));
        fp = fopen(filename, "r+");
        dir_read(dir, &f_array, fp);
        fclose(fp);

        for(i = 0; i < dir->linkcount; i++)
        {
            if(strcmp(f_array[i].filename, token) == 0)
            {
            // attempting to navigate to a file? Case 4, 5 or 6
                if(strcmp(f_array[i].type, "f") == 0)
                {
                    wasFile = 1;
                }
                if(strcmp(f_array[i].type, "l") == 0)
                {
                    wasLink = 1;
                }
                curBlock = f_array[i].blocknum;
                break;
            }
        }
    /* END SEARCH */

    // could not locate a blocknum: Case 2(missing dir) or 5(missing file)
        if(curBlock == prevBlock && strcmp(token, "..") != 0)
        {
            notExist = 1;
        }

    /* prep next file */
    // note that if curBlock == prevBlock, the next iteration will end early
        sprintf(filename, "%s%d", BLOCKPATHHEAD, curBlock);

        previousDir = currentDir;
        currentDir = token;
        token = strtok_r(NULL, "/", &rest);
    }
/* END PARSE
 * Return results
 */

    char* tempParentName    = calloc(200, 1);
    char* tempChildName     = calloc(200, 1);
    sprintf(tempParentName, "%s", previousDir);
    sprintf(tempChildName, "%s", currentDir);

    p->parentName   = tempParentName;
    p->parentBlock  = prevBlock;
    p->childName    = tempChildName;
    p->childBlock   = curBlock;
    p->isFile       = wasFile;
    p->isLink       = wasLink;



    if(dir != NULL && f_array != NULL)  // free can work on NULL but dir
    {                                   // and f_array are accessed
        for(i = 0; i < dir->linkcount; i++)
        {
            fi_deconstruct(&(f_array[i]));
        }
        free(f_array);
        free(dir);
    }

    return 0;
 }


int reserve(int* blocks, unsigned int count)
{
    if (blocks == NULL)
    {
        return 0;
    }

    if(FREEBLOCKSCOUNT < count)
    {
        return -1;
    }

    char filename[200];
    FILE* fp;
    unsigned int blocksindex = 0;

    // for reading
    int i;
    int j;
    char* rest;
    char* in = calloc(3000, 1);
    memset(in, '\0', 3000);
    int temp[400];
    int blocknum;
    int update;
    // for writing later
    int writeblocknum;
    char* out = calloc(3000, 1);
    memset(out, '\0', 3000);

    for(i = 1; i < 26 && blocksindex < count; i++)
    {
    // current file
        sprintf(filename, "%s%d", BLOCKPATHHEAD, i);
        fp = fopen(filename, "r+");
    // reading
        sprintf(in, "%s", ""); // wipe out string
        fgets(in, 3000, fp);
    // flag for checking whether or not to update afterwards
        update = 0;
    // first index
        blocknum = atoi(strtok_r(in, ",", &rest));
        for(j = 0; j < 399; j++)
        {
            temp[j] = atoi(strtok_r(NULL, ",", &rest));
        // free block and we need a block
            if(temp[j] == 1 && blocksindex < count)
            {
                blocks[blocksindex++] = blocknum;
                temp[j] = 0;
                update = 1;
            }
        // read next index
            blocknum = atoi(strtok_r(NULL, ",", &rest));
        }

    // for the final block
        temp[j] = atoi(strtok_r(NULL, ",", &rest));
        if(temp[j] == 1 && blocksindex < count)
        {
            blocks[blocksindex++] = blocknum;
            temp[j] = 0;
            update = 1;
        }
    /* end block reading */
    /* start block writing */
        if (update == 1)
        {
            sprintf(out, "%s", ""); // wipe out string
            writeblocknum = (i-1)*400;
            for(j = 0; j < 400; j++)
            {
                sprintf(out + strlen(out), "%d,%d,", writeblocknum, temp[j]);
                writeblocknum++;
            }
            sprintf(out + strlen(out) - 1, "%s", "\0"); // remove final comma
            fseek(fp, 0, SEEK_SET);
            fputs(out, fp);
        }
    /* end block writing */
        fclose(fp);
    }

    free(in);
    free(out);

    FREEBLOCKSCOUNT -= count;
    return 0;
}

int compare_int(const void* lh, const void* rh)
{
    if(*(int*)lh < *(int*)rh)
        return -1;
    return *(int*)lh != *(int*)rh;
}

void freeup(int** blockstofree, unsigned int count)
{
    int* blocks = *blockstofree;
    if (blocks == NULL)
    {
        return;
    }

    qsort(blocks, count, sizeof(int), compare_int);

    char filename[200] = "";
    FILE* fp;

    // for reading
    int i;
    int j;
    int filenum;
    char* rest;
    char* in = calloc(3000, 1);
    memset(in, '\0', 3000);
    int temp[400];
    int blocknum;
    int blocksindex = 0;
    // for writing later
    int writeblocknum;
    char* out = calloc(3000, 1);
    memset(out, '\0', 3000);

    for(i = 0; i < 26 && blocksindex < count; i++)
    {
        filenum = blocks[i]/400 + 1;
    // current file
        sprintf(filename, "%s%d", BLOCKPATHHEAD, filenum);
        fp = fopen(filename, "r+");
    // reading
        sprintf(in, "%s", ""); // wipe out string
        fgets(in, 3000, fp);
    // first index
        blocknum = atoi(strtok_r(in, ",", &rest));
        for(j = 0; j < 399; j++)
        {
            temp[j] = atoi(strtok_r(NULL, ",", &rest));
            if(blocksindex < count && blocknum == blocks[blocksindex])
            {
                temp[j] = 1;
                blocksindex++;
            }
        // read next index
            blocknum = atoi(strtok_r(NULL, ",", &rest));
        }

    // for the final block
        temp[j] = atoi(strtok_r(NULL, ",", &rest));
        if(blocksindex < count && blocknum == blocks[blocksindex])
        {
            temp[j] = 1;
            blocksindex++;
        }
    /* end block reading */
    /* start block writing */
        sprintf(out, "%s", ""); // wipe out string
        writeblocknum = (filenum-1)*400;
        for(j = 0; j < 400; j++)
        {
            sprintf(out + strlen(out), "%d,%d,", writeblocknum, temp[j]);
            writeblocknum++;
        }
        sprintf(out + strlen(out) - 1, "%s", "\0"); // remove final comma
        rewind(fp);
        fputs(out, fp);
    /* end block writing */
        fclose(fp);
    }

    free(in);
    free(out);

    FREEBLOCKSCOUNT += count;
}


static int slam_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    int res = -EPERM;

    if(FREEBLOCKSCOUNT < 2)
    {
        return -ENOSPC;
    }

    // 1. I'll need the parent directory
    parseReturn* p = malloc(sizeof(parseReturn));
    int result = parsePath(path, p);
    if(result != 0)
    {
        return result; // in case we encounter a parent that's a file
    }
    if(p->childBlock == p->parentBlock)         // creating something new
    {                                           // don't need to check isFile
                                                // CREATE only called on create file
        int* blocks = calloc(2, sizeof(int));    // need 2 block
                                                // one for inode, one for file
        reserve(blocks, 2);

        char filename[200] = "";
        sprintf(filename, "%s%d", BLOCKPATHHEAD, blocks[0]);
        FILE* fp = fopen(filename, "w+"); // CREATE CHILD INODE

        // CHILD:
        Inode* i = (Inode*)calloc(1, sizeof(Inode));
            i->size         = 0;
            i->uid          = geteuid();
            i->gid          = getegid();
            i->mode         = S_IFREG | mode;
            i->linkcount    = 1;
            i->atime        = time(NULL);
            i->ctime        = time(NULL);
            i->mtime        = time(NULL);
            i->indirect     = 0;
            i->location     = blocks[1];

        inode_write(i, fp);
        INODECOUNT += 1;
        free(i);
        fclose(fp);

        // PARENT:
        sprintf(filename, "%s%d", BLOCKPATHHEAD, p->parentBlock);
        fp = fopen(filename, "r+"); // DO NOT DISCARD, READ

        Directory* dir = (Directory*)calloc(1, sizeof(Directory));
        Filename_Inode* f_array;
        dir_read(dir, &f_array, fp);

        // append new FILE
        fi_build(&f_array[dir->linkcount], "f", p->childName, blocks[0]);
        dir->linkcount = dir->linkcount+1;
        fclose(fp);

        fp = fopen(filename, "w+"); // NOW DISCARD
        dir_write(dir, f_array, fp);
        fclose(fp);

// FREEING THINGS UP
        if(dir != NULL && f_array != NULL)  // free can work on NULL but dir
        {                                   // and f_array are accessed
            int i;
            for(i = 0; i < dir->linkcount; i++)
            {
                fi_deconstruct(&(f_array[i]));
            }
            free(f_array);
            free(dir);
        }

        free(blocks);


        res = 0;
    }

    pr_deconstruct(p);
    free(p);

    return res;
}
static void slam_destroy(void* private_data)
{
    free(SUPERBLOCK);

    char filename[200] = "";
    int i;
    for(i = 0; i < 10000; i++)
    {
        sprintf(filename, "%s%d", BLOCKPATHHEAD, i);
        remove(filename);
    }

}

static int slam_getattr(const char *path, struct stat *stbuf)
{
	printf("%s\n", "I am GETATTR");
	printf("%s\n", "I am GETATTR");
	printf("%s\n", "I am GETATTR");

	/*
	struct stat {
    dev_t     st_dev;     // ID of device containing file
    ino_t     st_ino;     // inode number
    mode_t    st_mode;    // protection
    nlink_t   st_nlink;   // number of hard links
    uid_t     st_uid;     // user ID of owner
    gid_t     st_gid;     // group ID of owner
    dev_t     st_rdev;    // device ID (if special file)
    off_t     st_size;    // total size, in bytes
    blksize_t st_blksize; // blocksize for file system I/O
    blkcnt_t  st_blocks;  // number of 512B blocks allocated
    time_t    st_atime;   // time of last access
    time_t    st_mtime;   // time of last modification
    time_t    st_ctime;   // time of last status change
    };
    */

    /*
    NO NANOSECOND SUPPORT
    struct timespec atime;
    struct timespec ctime;
    struct timespec mtime;
    */

	memset(stbuf, 0, sizeof(struct stat));

	if (strcmp(path, "/") == 0)
	{
        FILE* fp = fopen(ROOTBLOCKPATH, "r+");

        Directory* d = calloc(1, sizeof(Directory));
        Filename_Inode* f_array = NULL;
        dir_read(d, &f_array, fp);

            stbuf->st_dev       = SUPERBLOCK->devId;
            stbuf->st_ino       = SUPERBLOCK->root;
            stbuf->st_mode      = d->mode;
            stbuf->st_nlink     = d->linkcount;
            stbuf->st_uid       = d->uid;
            stbuf->st_gid       = d->gid;
            stbuf->st_size      = DIRECTORYSIZE;
            stbuf->st_blksize   = SUPERBLOCK->blockSize;
            stbuf->st_blocks    = (DIRECTORYSIZE-1)/(SUPERBLOCK->blockSize) +1;

            stbuf->st_atime     = d->atime;
            stbuf->st_ctime     = d->ctime;
            stbuf->st_mtime     = d->mtime;

		free(f_array);
		free(d);

		fclose(fp);
	}
	else
	{
        parseReturn* p = calloc(1, sizeof(parseReturn));
        int result = parsePath(path, p);
        if(result != 0)
        {
            return result;
        }
        if(p->childBlock == p->parentBlock)
        {
            return -ENOENT;
        }

        char filename[200] = "";
        sprintf(filename, "%s%d", BLOCKPATHHEAD, p->childBlock);
        FILE* fp = fopen(filename, "r+"); // READ ONLY


    // A FILE
        if(p->isFile || p->isLink)
        {
            Inode* i = (Inode*)calloc(1, sizeof(Inode));
            inode_read(i, fp);
            fclose(fp);

                stbuf->st_dev       = SUPERBLOCK->devId;
                stbuf->st_ino       = p->childBlock;
                stbuf->st_mode      = i->mode;
                stbuf->st_nlink     = i->linkcount;
                stbuf->st_uid       = i->uid;
                stbuf->st_gid       = i->gid;
                stbuf->st_size      = i->size;
                stbuf->st_blksize   = SUPERBLOCK->blockSize;
            if(i->size == 0)
                stbuf->st_blocks    = 0;
            else
                stbuf->st_blocks    = (i->size-1)/(SUPERBLOCK->blockSize) +2;

                stbuf->st_atime     = i->atime;
                stbuf->st_ctime     = i->ctime;
                stbuf->st_mtime     = i->mtime;


            if(i->indirect == 1)
            {
                stbuf->st_blocks    = (i->size-1)/(SUPERBLOCK->blockSize) +3;
            }

            free(i);
        }

    // A DIRECTORY
        else
        {
            Directory* dir = (Directory*)calloc(1, sizeof(Directory));
            Filename_Inode* f_array;
            dir_read(dir, &f_array, fp);
            fclose(fp);

                stbuf->st_dev       = SUPERBLOCK->devId;
                stbuf->st_ino       = p->childBlock;
                stbuf->st_mode      = dir->mode;
                stbuf->st_nlink     = dir->linkcount;
                stbuf->st_uid       = dir->uid;
                stbuf->st_gid       = dir->gid;
                stbuf->st_size      = DIRECTORYSIZE;
                stbuf->st_blksize   = SUPERBLOCK->blockSize;
                stbuf->st_blocks    = (DIRECTORYSIZE-1)/(SUPERBLOCK->blockSize) +1;

                stbuf->st_atime     = dir->atime;
                stbuf->st_ctime     = dir->ctime;
                stbuf->st_mtime     = dir->mtime;


            if(dir != NULL && f_array != NULL)  // free can work on NULL but dir
            {                                   // and f_array are accessed
                int i;
                for(i = 0; i < dir->linkcount; i++)
                {
                    fi_deconstruct(&(f_array[i]));
                }
                free(f_array);
                free(dir);
            }
        }

        pr_deconstruct(p);
        free(p);

	}

	return 0;
}
static void* slam_init(struct fuse_conn_info *conn)
{
	FILE* fp;
    char filename[200] = "";

    char zero[1] = "\0";

    int i;
    for(i = 0; i < 10000; i++)
    {
        sprintf(filename, "%s%d", BLOCKPATHHEAD, i);
        fp = fopen(filename, "w+");
        fseek(fp, 4*1024-1, SEEK_SET);
        fwrite(zero, 1, 1, fp);
        fclose(fp);
    }

/* ===== SUPERBLOCK ===== */
    SUPERBLOCK = (Superblock*)calloc(1, sizeof(Superblock));
        SUPERBLOCK->creationTime    = time(NULL);
        SUPERBLOCK->mounted         = 1;
        SUPERBLOCK->devId           = 20;
        SUPERBLOCK->freeStart       = 1;
        SUPERBLOCK->freeEnd         = 26;
        SUPERBLOCK->root            = 26;
        SUPERBLOCK->maxBlocks       = 10000;
        SUPERBLOCK->blockSize       = 4096;
        SUPERBLOCK->maxFilename     = 100;
        SUPERBLOCK->maxFileSize     = 400*4096;

    fp = fopen("/fusedata/fusedata.0", "w+");
    fwrite(SUPERBLOCK, sizeof(Superblock), 1, fp);
    fclose(fp);

/* ===== FREE BLOCKS 1-25 ===== */
    // BLOCKS 0-399

    sprintf(filename, "%s%d", BLOCKPATHHEAD, 1);
    fp = fopen(filename, "w");
    char* out = malloc(3000);
    sprintf(out, "%s", "");     // otherwise out starts off with \n
    int h;
    for(h = 0; h < 27; h++)          // blocks 0-26
    {
        sprintf(out + strlen(out), "%d,%d,", h, 2);
    }
    for(h = 27; h < 400; h++)   // blocks 27-399
    {
        sprintf(out + strlen(out), "%d,%d,", h, 1);
    }
    sprintf(out + strlen(out) - 1, "%s", "\0"); // remove final comma
    fputs(out, fp);
    free(out);
    fclose(fp);

    // THE REST OF THE BLOCKS (400-9999):
    int j;
    int jmax;
    int jmin;
    for(i = 2; i < 26; i++)
    {
        sprintf(filename, "%s%d", BLOCKPATHHEAD, i);
        fp = fopen(filename, "w");
        out = malloc(3000);
        sprintf(out, "%s", "");
        jmax = i*400;       // this can be in for loop but then it would be
                            // recalculated every interval (this is better)
        jmin = (i-1)*400;   // just for clarity
        for(j = jmin; j < jmax; j++)
        {
            sprintf(out + strlen(out), "%d,%d,", j, 1);
        }
        sprintf(out + strlen(out) - 1, "%s", "\0"); // remove final comma
        fputs(out, fp);
        free(out);
        fclose(fp);
    }

    FREEBLOCKSCOUNT = 10000-27;
    INODECOUNT = 0;

/* ===== ROOT DIRECTORY ===== */
    Directory *dir = (Directory*)calloc(1, sizeof(Directory));
        dir->size                   = 1033;
        dir->uid                    = geteuid();
        dir->gid                    = getegid();
        dir->mode                   = S_IFDIR | 0775;
        dir->atime                  = time(NULL);
        dir->ctime                  = time(NULL);
        dir->mtime                  = time(NULL);
        dir->linkcount              = 2;

    Filename_Inode* f_array = malloc(sizeof(Filename_Inode)*(dir->linkcount));

    // root block #
    ROOTBLOCK = SUPERBLOCK->root;
    ROOTBLOCKPATH = malloc(50);
    sprintf(ROOTBLOCKPATH, "%s%d", BLOCKPATHHEAD, ROOTBLOCK);

    fi_build(&f_array[0], "d", ".", ROOTBLOCK);

    fi_build(&f_array[1], "d", "..", ROOTBLOCK);


    // open and write
    fp = fopen(ROOTBLOCKPATH, "w+");
    dir_write(dir, f_array, fp);
    fclose(fp);

    int* blocks = calloc(10, sizeof(int));
    reserve(blocks, 10);
    int* blocksup = malloc(10*sizeof(int));
    blocksup[0] = 32;
    blocksup[1] = 29;
    blocksup[2] = 36;
    blocksup[3] = 27;
    blocksup[4] = 28;
    blocksup[5] = 30;
    blocksup[6] = 35;
    blocksup[7] = 33;
    blocksup[8] = 31;
    blocksup[9] = 34;
    freeup(&blocksup, 10);
    free(blocks);

    return 0;
}
static int slam_link(const char *from, const char *to)
{
    if(FREEBLOCKSCOUNT == 0)
    {
        return -ENOSPC;
    }

    parseReturn* p_from = (parseReturn*)calloc(1, sizeof(parseReturn));
    parsePath(from, p_from);

    parseReturn* p_to = (parseReturn*)calloc(1, sizeof(parseReturn));
    parsePath(to, p_to);

    if(p_from->parentBlock == p_from->childBlock)
    {
        pr_deconstruct(p_from);
        pr_deconstruct(p_to);
        free(p_from);
        free(p_to);

        return -ENOENT;
    }
    if(!(p_from->isFile))
    {
        pr_deconstruct(p_from);
        pr_deconstruct(p_to);
        free(p_from);
        free(p_to);

        return -EPERM;
    }

// should check for write permissions at some point

// open parent directory and add the entry:
    char filename[200] = "";
    // from is only needed for info, the to is where we add a file
    sprintf(filename, "%s%d", BLOCKPATHHEAD, p_to->parentBlock);
    FILE* fp = fopen(filename, "r+");

    Directory* dir = (Directory*)calloc(1, sizeof(Directory));
    Filename_Inode* f_array = NULL;
    dir_read(dir, &f_array, fp);
    fclose(fp);
// hard links only work for files

        fi_build(&f_array[dir->linkcount], "f", p_to->childName, p_from->childBlock);

        dir->linkcount += 1;

// update parent
    fp = fopen(filename, "w+");
    // ctime should change on entry addition:
    dir->ctime = time(NULL);
    dir_write(dir, f_array, fp);
    fclose(fp);

// update file link count
    sprintf(filename, "%s%d", BLOCKPATHHEAD, p_from->childBlock);
    fp = fopen(filename, "r+");

    Inode* ino = (Inode*)calloc(1, sizeof(Inode));
    inode_read(ino, fp);

        ino->linkcount += 1;

        // update ctime
        ino->ctime = time(NULL);

    rewind(fp);
    inode_write(ino, fp);
    fclose(fp);

    free(ino);
    int i;
    for(i = 0; i < dir->linkcount; i++)
    {
        fi_deconstruct(&f_array[i]);
    }
    free(f_array);
    free(dir);

    pr_deconstruct(p_from);
    pr_deconstruct(p_to);
    free(p_from);
    free(p_to);


    return 0;
}
/* Very Interesting
 * On mkdir FUSE calls: getattr->mkdir->getattr
 * If the first getattr passes, then FUSE automatically responds with error
 * File exists
 */
static int slam_mkdir(const char *path, mode_t mode)
{
    if(FREEBLOCKSCOUNT == 0)
    {
        return -ENOSPC;
    }

    if(strcmp(path, "/") == 0)
    {
        return -EISDIR;
    }

    parseReturn* p = malloc(sizeof(parseReturn));
    parsePath(path, p);
/* CASE 1:
 * reached the end and didn't find a non-existing dir?
 */
    if(p->parentBlock != p->childBlock)
    {
        return -EISDIR;
    }

/* CASE 2:
 * Everything checks out, make a new CHILD directory
 */

// 1. reserve a block
    int* block  = calloc(1, sizeof(int));
    reserve(block, 1);

    int newBlockNum = block[0];
// 2. create new directory object
//    for fusedata.newBlockNum
    Directory* dir = (Directory*)calloc(1, sizeof(Directory));
        dir->size                   = 1033;
        dir->uid                    = geteuid();
        dir->gid                    = getegid();
        dir->mode                   = S_IFDIR | mode;
        dir->atime                  = time(NULL);
        dir->ctime                  = time(NULL);
        dir->mtime                  = time(NULL);
        dir->linkcount              = 2;

    Filename_Inode* f_array = malloc(dir->linkcount*sizeof(Filename_Inode));

    fi_build(&f_array[0], "d", ".", newBlockNum);
    fi_build(&f_array[1], "d", "..", p->parentBlock);
// 3. Open and write to fusedata.newBlockNum
    char filename[200] = "";
    sprintf(filename, "%s%d", BLOCKPATHHEAD, newBlockNum);
    FILE* fp = fopen(filename, "w+"); // discard contents
    dir_write(dir, f_array, fp);
    fclose(fp);
    int i;
    for(i = 0; i < dir->linkcount; i++)
    {
       fi_deconstruct(&(f_array[i]));
    }
    free(f_array);
    free(dir);
/* UPDATE PARENT */
// 4. Open and write to PARENT aka fusedata.p->parentBlock
    sprintf(filename, "%s%d", BLOCKPATHHEAD, p->parentBlock);
    fp = fopen(filename, "r+"); // DO NOT DISCARD, READ

    dir = (Directory*)calloc(1, sizeof(Directory));
    dir_read(dir, &f_array, fp);

    // append new directory
    fi_build(&f_array[dir->linkcount], "d", p->childName, newBlockNum);
    dir->linkcount = dir->linkcount+1;
    fclose(fp);

    fp = fopen(filename, "w+"); // NOW DISCARD
    // ctime should update on append entry
    dir->ctime = time(NULL);
    dir_write(dir, f_array, fp);
    fclose(fp);

    for(i = 0; i < dir->linkcount; i++)
    {
       fi_deconstruct(&(f_array[i]));
    }
    free(f_array);
    free(dir);

    pr_deconstruct(p);
    free(p);


    free(block);

    return 0;
}
static int slam_open(const char *path, struct fuse_file_info *fi)
{
    parseReturn* p = calloc(1, sizeof(parseReturn));
    int result = parsePath(path, p);
    if(result != 0)
    {
        pr_deconstruct(p);
        free(p);
        return result;
    }
    if(p->childBlock == p->parentBlock)
    {
        pr_deconstruct(p);
        free(p);
        return -ENOENT;
    }

// A FILE
    if(p->isFile == 1)
    {
        unsigned long mode;
        unsigned int gid;
        unsigned int uid;
        unsigned int childBlock;

        char filename[200] = "";
        sprintf(filename, "%s%d", BLOCKPATHHEAD, p->childBlock);
        FILE* fp = fopen(filename, "r+"); // READ ONLY

        Inode* i = (Inode*)calloc(1, sizeof(Inode));
        inode_read(i, fp);
        fclose(fp);

        mode        = i->mode;
        uid         = i->uid;
        gid         = i->gid;
        childBlock  = p->childBlock;

        pr_deconstruct(p);
        free(p);
        free(i);

        sprintf(filename, "%s%d", BLOCKPATHHEAD, childBlock);

        if ((fi->flags & 3) == O_RDONLY)
        {
            if(geteuid() == uid)
            {
                if((U_READ & mode) != U_READ)
                    return -EACCES;
                fp = fopen(filename, "r+");
                if(fi->fh == 0)
                {
                    file_release* f = (file_release*)calloc(1, sizeof(file_release));
                    f->fp = fp;
                    f->output = NULL;
                    fi->fh = (u_int64_t)f;
                }
                ((file_release*)(fi->fh))->fp = fp;

                return 0;
            }
            else if(getegid() == gid)
            {
                if((G_READ & mode) != G_READ)
                    return -EACCES;

                fp = fopen(filename, "r+");
                if(fi->fh == 0)
                {
                    file_release* f = (file_release*)calloc(1, sizeof(file_release));
                    f->fp = fp;
                    f->output = NULL;
                    fi->fh = (u_int64_t)f;
                }
                ((file_release*)(fi->fh))->fp = fp;

                return 0;
            }
            else if((O_READ & mode) == O_READ)
                return -EACCES;

            fp = fopen(filename, "r+");
            if(fi->fh == 0)
            {
                file_release* f = (file_release*)calloc(1, sizeof(file_release));
                f->fp = fp;
                f->output = NULL;
                fi->fh = (u_int64_t)f;
            }
            ((file_release*)(fi->fh))->fp = fp;
        }
        // when writing will have to update the size field
        if ((fi->flags & 3) == O_WRONLY || (fi->flags & 3) == O_RDWR)
        {
            // permissions are hierchical no way around this...
            if(geteuid() == uid)
            {
                if((U_WRITE & mode) != U_WRITE || (U_READ & mode) != U_READ)
                    return -EACCES;
                fp = fopen(filename, "r+");
                if(fi->fh == 0)
                {
                    file_release* f = (file_release*)calloc(1, sizeof(file_release));
                    f->fp = fp;
                    f->output = NULL;
                    fi->fh = (u_int64_t)f;
                }
                ((file_release*)(fi->fh))->fp = fp;

                return 0;
            }
            else if(getegid() == i->gid)
            {
                if((G_WRITE & mode) != G_WRITE || (G_READ & mode) != G_READ)
                    return -EACCES;

                fp = fopen(filename, "r+");
                if(fi->fh == 0)
                {
                    file_release* f = (file_release*)calloc(1, sizeof(file_release));
                    f->fp = fp;
                    f->output = NULL;
                    fi->fh = (u_int64_t)f;
                }
                ((file_release*)(fi->fh))->fp = fp;

                return 0;
            }
            else if((O_WRITE & mode) != O_WRITE || (O_READ & mode) == O_READ)
                return -EACCES;

            fp = fopen(filename, "r+");
            if(fi->fh == 0)
            {
                file_release* f = (file_release*)calloc(1, sizeof(file_release));
                f->fp = fp;
                f->output = NULL;
                fi->fh = (u_int64_t)f;
            }
            ((file_release*)(fi->fh))->fp = fp;
        }

        return 0;
    }

	return -EPERM;
}
// S_IRUSR, S_IRGRP, S_IROTH
static int slam_opendir(const char* path, struct fuse_file_info* fi)
{
    printf("%s\n", "I am OPENDIR");
    printf("%s\n", "I am OPENDIR");
    printf("%s\n", "I am OPENDIR");

    unsigned long mode;
    unsigned int gid;
    unsigned int uid;

    if (strcmp(path, "/") == 0)
	{
        FILE* fp = fopen(ROOTBLOCKPATH, "r+");

        Directory* d = malloc(sizeof(Directory));
        Filename_Inode* f_array = NULL;
        dir_read(d, &f_array, fp);

        mode    = d->mode;
        gid     = d->gid;
        uid     = d->uid;

		free(f_array);
		free(d);
        if(fi->fh == 0)
        {
            file_release* f = (file_release*)calloc(1, sizeof(file_release));
            f->fp = fp;
            f->output = NULL;
            fi->fh = (u_int64_t)f;
        }
        ((file_release*)(fi->fh))->fp = fp;

        if(geteuid() == uid)
        {
            if((U_READ & mode) != U_READ)
                return -EACCES;

            return 0;
        }
        else if(getegid() == gid)
        {
            if((G_READ & mode) != G_READ)
                return -EACCES;

            return 0;
        }
        else if((O_READ & mode) == O_READ)
            return -EACCES;

	}
	else
	{

        parseReturn* p = malloc(sizeof(parseReturn));
        int result = parsePath(path, p);
        if(result != 0)
        {
            return result;
        }
        if(p->childBlock == p->parentBlock)
        {
            return -ENOENT;
        }

        // for now only dealing with directories
        if(p->isFile == 1)
        {
            return -ENOENT;
        }

        char filename[200] = "";
        sprintf(filename, "%s%d", BLOCKPATHHEAD, p->childBlock);
        FILE* fp = fopen(filename, "r+"); // READ ONLY

        Directory* dir = (Directory*)calloc(1, sizeof(Directory));
        Filename_Inode* f_array;
        dir_read(dir, &f_array, fp);
        if(fi->fh == 0)
        {
            file_release* f = (file_release*)calloc(1, sizeof(file_release));
            f->fp = fp;
            f->output = NULL;
            fi->fh = (u_int64_t)f;
        }
        ((file_release*)(fi->fh))->fp = fp;

        mode    = dir->mode;
        uid     = dir->uid;
        gid     = dir->gid;

        if(dir != NULL && f_array != NULL)  // free can work on NULL but dir
        {                                   // and f_array are accessed
            int i;
            for(i = 0; i < dir->linkcount; i++)
            {
                fi_deconstruct(&(f_array[i]));
            }
            free(f_array);
            free(dir);
        }

        pr_deconstruct(p);
        free(p);

        if(geteuid() == uid)
        {
            if((U_READ & mode) != U_READ)
                return -EACCES;

            return 0;
        }
        else if(getegid() == gid)
        {
            if((G_READ & mode) != G_READ)
                return -EACCES;

            return 0;
        }
        else if((O_READ & mode) == O_READ)
            return -EACCES;
    }


    return 0;
}
static int slam_read(const char *path, char *output, size_t size, off_t offset,
		    struct fuse_file_info *fi)
{
/* Get the pointer to the file:
 */
    FILE* fp;
    if(fi->fh == 0) // wow... unreliable FUSE...
    {
        parseReturn* p = malloc(sizeof(parseReturn));
        parsePath(path, p);
        char* inode_file = malloc(200);
        sprintf(inode_file, "%s%d", BLOCKPATHHEAD, p->childBlock);

        fp = fopen(inode_file, "r+");
        free(inode_file);

        file_release* f = (file_release*)calloc(1, sizeof(file_release));
        f->fp = fp;
        f->output = NULL;
        fi->fh = (u_int64_t)f;
    }
    fp = ((file_release*)(fi->fh))->fp;

/* Rewind and read the inode:
 */
    rewind(fp);
	Inode* i = (Inode*)calloc(1, sizeof(Inode));
	inode_read(i, fp);

	// atimes get updated on read:
	i->atime = time(NULL);
	rewind(fp);
	inode_write(i, fp);
	rewind(fp);

/* Prep to get the actual raw file:
 */
	char filename[200] = "";
	sprintf(filename, "%s%d", BLOCKPATHHEAD, i->location);
	FILE* fp2 = fopen(filename, "r+");

    // if you want to read past the size that's a no
    if(offset > size)
    {
        fclose(fp2);
        free(i);
        return -EPERM;
    }

    // if size is 0 or theres the offset is the end of the file:
    if(size == 0 || offset == size)
    {
        fclose(fp2);
        free(i);
        return 0;
    }

    int numBlocks = (i->size-1)/(SUPERBLOCK->blockSize) + 2;

    // create a buffer to read into based on the size:
    char* filestream = calloc(numBlocks * (SUPERBLOCK->blockSize), 1);
    memset(filestream, '\0', numBlocks * (SUPERBLOCK->blockSize));

/* in the case of non-indirect:
 */
    if(i->indirect != 1)
    {
    // read
        fread(filestream, 1, size, fp2);

    // copy into the output buffer
    /* CHECK: file has: [a|b|c|\n|d], [0|1|2|3|4]
     * size = 5, offset = 3
     * output would be: [\n|d]
     * filestream + offset = (0 + 3) = 3
     * output length is then 2 (size - offset)(5-2)
     */
     //         dest         source            size
        memcpy(output, filestream + offset, size-offset);

        fclose(fp2);
        free(i);
        free(filestream);
        return (size-offset);
    }

/* in the case of indirect:
 */
    // read the index file:
    // the raw file, fp2 here is an index file
    int blocks[400];
    index_read(blocks, fp2);

    // read into the buffer:
    char staging_buf[4096] = "";
    FILE* fp3;
    int j;
    int jmax = (i->size-1)/(SUPERBLOCK->blockSize) + 1;
    /* CHECK: 350 blocks = 4096 * 350 = 1433600,
     * 1433600-1 = 1433599, 1433599/4096 = 349 (int truncate), 349+1 = 350
     */
    for(j = 0; j < jmax; j++)
    {
    // open
        sprintf(filename, "%s%d", BLOCKPATHHEAD, blocks[j]);
        fp3 = fopen(filename, "r+");
    // read
        fread(staging_buf, 1, 4096, fp3);
        fclose(fp3);
    // always copy entire blocks:
        memcpy(filestream + (j*4096), staging_buf, 4096);
    }
    // output:
    memcpy(output, filestream + offset, size-offset);
    //      dest         source            size

    fclose(fp2);
    free(i);
    ((file_release*)(fi->fh))->output = filestream;
    return (size-offset);
}
static int slam_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
		       off_t offset, struct fuse_file_info *fi)
{
    (void) offset;
	(void) fi;

	FILE* fp = ((file_release*)(fi->fh))->fp;

    if(strcmp(path, "/") == 0)
    {
        if(fp == NULL)
        {
            FILE* fp = fopen(ROOTBLOCKPATH, "r+");
            if(fi->fh == 0)
            {
                file_release* f = (file_release*)calloc(1, sizeof(file_release));
                f->fp = fp;
                f->output = NULL;
                fi->fh = (u_int64_t)f;
            }
            ((file_release*)(fi->fh))->fp = fp;
        }
        rewind(fp);
        Directory* d = malloc(sizeof(Directory));
        Filename_Inode* f_array = NULL; // initialize the pointer to point to NULL
        dir_read(d, &f_array, fp);      // pass in the address of the pointer itself

        // atimes get updated on read:
        d->atime = time(NULL);
        rewind(fp);
        dir_write(d, f_array, fp);
        rewind(fp);

        int i;
        for(i = 0; i < d->linkcount; i++)
        {
            //printf("%s\n", f_array[i].filename);
            filler(buf, f_array[i].filename, NULL, 0);
            fi_deconstruct(&(f_array[i]));
        }
        free(f_array);
        free(d);
	}
	else
	{
        if(fp == NULL)
        {
            parseReturn* p = malloc(sizeof(parseReturn));
            int result = parsePath(path, p);
            if(result != 0)
            {
                free(p);
                return result;
            }
            if(p->childBlock == p->parentBlock)
            {
                free(p);
                return -ENOENT;
            }

            char filename[200] = "";
            sprintf(filename, "%s%d", BLOCKPATHHEAD, p->childBlock);
            FILE* fp = fopen(filename, "r+"); // READ ONLY
            if(fi->fh == 0)
            {
                file_release* f = (file_release*)calloc(1, sizeof(file_release));
                f->fp = fp;
                f->output = NULL;
                fi->fh = (u_int64_t)f;
            }
            ((file_release*)(fi->fh))->fp = fp;

            pr_deconstruct(p);
            free(p);
        }

        rewind(fp);
        Directory* dir = (Directory*)calloc(1, sizeof(Directory));
        Filename_Inode* f_array;
        dir_read(dir, &f_array, fp);

        // atimes get updated on read:
        dir->atime = time(NULL);
        fflush(fp);
        fseek(fp, 0, SEEK_SET);
        dir_write(dir, f_array, fp);
        rewind(fp);

        int i;
        for(i = 0; i < dir->linkcount; i++)
        {
            filler(buf, f_array[i].filename, NULL, 0);
        }

        if(dir != NULL && f_array != NULL)  // free can work on NULL but dir
        {                                   // and f_array are accessed
            int i;
            for(i = 0; i < dir->linkcount; i++)
            {
                fi_deconstruct(&(f_array[i]));
            }
            free(f_array);
            free(dir);
        }
	}

	return 0;

}
static int slam_readlink(const char *path, char *buf, size_t size)
{
    parseReturn* p = calloc(1, sizeof(parseReturn));
    int result = parsePath(path, p);
    if(result != 0)
    {
        pr_deconstruct(p);
        free(p);
        return result;
    }

    char filename[200] = "";
    sprintf(filename, "%s%d", BLOCKPATHHEAD, p->childBlock);
    FILE* fp = fopen(filename, "r+");

    Inode* i = calloc(1, sizeof(Inode));
    inode_read(i, fp);

    // atimes get updated on read:
    i->atime = time(NULL);
    rewind(fp);
    inode_write(i, fp);
    rewind(fp);

    fclose(fp);

    sprintf(filename, "%s%d", BLOCKPATHHEAD, i->location);
    fp = fopen(filename, "r+");

    char buffer[4096];
    memset(buffer, '\0', 4096);
    fread(buffer, 4096, 1, fp);
    fclose(fp);

    char* res = calloc(size, 1);
    memset(res, '\0', size);
    snprintf(res, i->size+1, "%s", buffer);

    memcpy(buf, res, size);
    free(res);

    return 0;
}
static int slam_release(const char *path, struct fuse_file_info *fi)
{
    (void)fi;

    if(fi->fh != 0)
    {
        file_release* f = (file_release*)(fi->fh);
        FILE* fp = f->fp;
        fclose(fp);
        if(f->output != NULL)
        {
            free(f->output);
        }
        free(f);
    }

    return 0;
}
static int slam_releasedir(const char* path, struct fuse_file_info *fi)
{
    (void)fi;

    if(fi->fh != 0)
    {
        file_release* f = (file_release*)(fi->fh);
        FILE* fp = f->fp;
        fclose(fp);
        free(f);
    }

    return 0;
}
static int slam_rename(const char *from, const char *to)
{
    parseReturn* p_from = (parseReturn*)calloc(1, sizeof(parseReturn));
    parsePath(from, p_from);

    parseReturn* p_to = (parseReturn*)calloc(1, sizeof(parseReturn));
    parsePath(to, p_to);

    if(p_from->parentBlock == p_from->childBlock)
    {
        return -ENOENT;
    }

    if(p_from->parentBlock != p_to->parentBlock)
    {
        return -ENOEXEC;
    }

// should check for write permissions at some point

// open parent directory and change the entry:
    char filename[200] = "";
    sprintf(filename, "%s%d", BLOCKPATHHEAD, p_from->parentBlock);
    FILE* fp = fopen(filename, "r+");

    Directory* dir = (Directory*)calloc(1, sizeof(Directory));
    Filename_Inode* f_array = NULL;
    dir_read(dir, &f_array, fp);
    fclose(fp);

    int i;
    for(i = 0; i < dir->linkcount; i++)
    {
        if(strcmp(f_array[i].filename, p_from->childName) == 0)
        {
            f_array[i].filename = p_to->childName;
            break;
        }
    }

// update
    fp = fopen(filename, "w+");
    // renaming should change the dir ctime
    dir->ctime = time(NULL);
    dir_write(dir, f_array, fp);
    fclose(fp);

    return 0;
}
static int slam_statfs(const char *path, struct statvfs *stbuf)
{
/*
    unsigned long f_bsize    File system block size.
    unsigned long f_frsize   Fundamental file system block size.
    fsblkcnt_t    f_blocks   Total number of blocks on file system in units of f_frsize.
    fsblkcnt_t    f_bfree    Total number of free blocks.
    fsblkcnt_t    f_bavail   Number of free blocks available to
                             non-privileged process.
    fsfilcnt_t    f_files    Total number of file serial numbers.
    fsfilcnt_t    f_ffree    Total number of free file serial numbers.
    fsfilcnt_t    f_favail   Number of file serial numbers available to
                             non-privileged process.
    unsigned long f_fsid     File system ID.
    unsigned long f_flag     Bit mask of f_flag values.
    unsigned long f_namemax  Maximum filename length.
*/
        stbuf->f_bsize      = SUPERBLOCK->blockSize;
        stbuf->f_frsize     = SUPERBLOCK->blockSize;
        stbuf->f_blocks     = SUPERBLOCK->maxBlocks;
        stbuf->f_bfree      = FREEBLOCKSCOUNT;
        stbuf->f_bavail     = FREEBLOCKSCOUNT;

        stbuf->f_files      = INODECOUNT;
        stbuf->f_ffree      = FREEBLOCKSCOUNT;
        stbuf->f_favail     = FREEBLOCKSCOUNT;

        stbuf->f_fsid       = SUPERBLOCK->devId;
        stbuf->f_namemax    = SUPERBLOCK->maxFilename;

    return 0;
}
static int slam_symlink(const char* target, const char* newfile)
{
    //essentially a create function but with a mode of symlink
        int res = -EPERM;

    if(FREEBLOCKSCOUNT < 2)
    {
        return -ENOSPC;
    }

    parseReturn* p_newfile = (parseReturn*)calloc(1, sizeof(parseReturn));
    parsePath(newfile, p_newfile);


    if(p_newfile->childBlock == p_newfile->parentBlock)  // creating something new
    {                                           // don't need to check isFile
                                                // CREATE only called on create file
        // get info about from
        char filename[200] = "";
        // make the to
        int* blocks = calloc(2, sizeof(int)); // need 2 block
                                                // one for inode, one for file
        reserve(blocks, 2);

        sprintf(filename, "%s%d", BLOCKPATHHEAD, blocks[0]);
        FILE* fp = fopen(filename, "w+"); // CREATE CHILD INODE

        // CHILD:
        Inode* i = (Inode*)calloc(1, sizeof(Inode));
            i->size         = strlen(target);
            i->uid          = geteuid();
            i->gid          = getegid();
            i->mode         = S_IFLNK | 0664;
            i->linkcount    = 1;
            i->atime        = time(NULL);
            i->ctime        = time(NULL);
            i->mtime        = time(NULL);
            i->indirect     = 0;
            i->location     = blocks[1];

        inode_write(i, fp);
        INODECOUNT += 1;
        free(i);
        fclose(fp);

        // WRITE TO FILE:
        sprintf(filename, "%s%d", BLOCKPATHHEAD, blocks[1]);
        fp = fopen(filename, "w+");
        char output[4096] = "";
        memset(output, '\0', 4096);

        sprintf(output, "%s", target);
        fwrite(output, 1, 4096, fp);
        fclose(fp);


        // PARENT:
        sprintf(filename, "%s%d", BLOCKPATHHEAD, p_newfile->parentBlock);
        fp = fopen(filename, "r+"); // DO NOT DISCARD, READ

        Directory* dir = (Directory*)calloc(1, sizeof(Directory));
        Filename_Inode* f_array;
        dir_read(dir, &f_array, fp);

        // append new FILE
        fi_build(&f_array[dir->linkcount], "l", p_newfile->childName, blocks[0]);
        dir->linkcount = dir->linkcount+1;
        fclose(fp);

        fp = fopen(filename, "w+"); // NOW DISCARD
        // ctime should update due to symlink entry addition:
        dir->ctime = time(NULL);
        dir_write(dir, f_array, fp);
        fclose(fp);

// FREEING THINGS UP
        if(dir != NULL && f_array != NULL)  // free can work on NULL but dir
        {                                   // and f_array are accessed
            int j;
            for(j = 0; j < dir->linkcount; j++)
            {
                fi_deconstruct(&(f_array[j]));
            }
            free(f_array);
            free(dir);
        }

        free(blocks);


        res = 0;
    }

    pr_deconstruct(p_newfile);
    free(p_newfile);

    return res;

}

static int slam_unlink(const char *path)
{
    parseReturn* p = (parseReturn*)calloc(1, sizeof(parseReturn));
    int result = parsePath(path, p);

    if(result != 0)
    {
        pr_deconstruct(p);
        free(p);
        return result;
    }
    if(p->parentBlock == p->childBlock)
    {
        pr_deconstruct(p);
        free(p);
        return -ENOENT;
    }

// not for dirs
    if(!(p->isFile || p->isLink))
    {
        pr_deconstruct(p);
        free(p);
        return -EPERM;
    }
// blocks to free at end
    int* blocks = (int*)calloc(1000, sizeof(int)); // 1000 blocks is a huge limit
    memset(blocks, 0, 1000*sizeof(int));
    unsigned int blocksindex = 0;

// read inode
    char filename[200] = "";
    sprintf(filename, "%s%d", BLOCKPATHHEAD, p->childBlock);

    FILE* fp = fopen(filename, "r+");
    Inode* i = (Inode*)calloc(1, sizeof(Inode));
    inode_read(i, fp);

    // dercrement link count
    i->linkcount -= 1;
    // ctimes are updated on linkcount changes
    i->ctime = time(NULL);
    rewind(fp);
    inode_write(i, fp);
    fclose(fp);

    // if there are no more links to this file
    if(i->linkcount == 0)
    {
        if(i->indirect)
        {
            sprintf(filename, "%s%d", BLOCKPATHHEAD, i->location);
            FILE* index_fp = fopen(filename, "r+");
            int raw_blocks[400];
            index_read(raw_blocks, index_fp);

            int q;
            for(q = 0; q < 400; q++)
            {
                if(raw_blocks[q] == -1)
                    continue;
                blocks[blocksindex++] = raw_blocks[q];
            }

        }

        blocks[blocksindex++] = i->location;    // delete the file data
        blocks[blocksindex++] = p->childBlock;  // delete the inode


    }

    free(i);

// remove link from parent:

    sprintf(filename, "%s%d", BLOCKPATHHEAD, p->parentBlock);

    fp = fopen(filename, "r+");

    Directory* dir = (Directory*)calloc(1, sizeof(Directory));
    Filename_Inode* f_array = NULL;
    dir_read(dir, &f_array, fp);

    Filename_Inode* newf_array = (Filename_Inode*)calloc(1, sizeof(Filename_Inode)*dir->linkcount-1);

    int j;
    int k = 0;
    for(j = 0; j < dir->linkcount; j++)
    {
        if(strcmp(f_array[j].filename, p->childName) != 0)
        {
            fi_build(&(newf_array[k++]), f_array[j].type, f_array[j].filename, f_array[j].blocknum);
        }
    }

    dir->linkcount -= 1;
    // ctimes are updated on linkcount changes
    dir->ctime = time(NULL);
    rewind(fp);
    dir_write(dir, newf_array, fp);
    fclose(fp);

// free up block

    freeup(&blocks, blocksindex);

    for(j = 0; j < dir->linkcount+1; j++)
    {
        fi_deconstruct(&f_array[j]);
    }
    free(f_array);
    for(j = 0; j < dir->linkcount; j++)
    {
        fi_deconstruct(&newf_array[j]);
    }
    free(newf_array);

    free(dir);
    free(blocks);

    return 0;
}
static int slam_write(const char *path, const char *input, size_t size,
		     off_t offset, struct fuse_file_info *fi)
{
/* Get the pointer to the file:
 */
    FILE* fp;
    if(fi->fh == 0) // wow... unreliable FUSE...
    {
        parseReturn* p = malloc(sizeof(parseReturn));
        parsePath(path, p);
        char* inode_file = malloc(200);
        sprintf(inode_file, "%s%d", BLOCKPATHHEAD, p->childBlock);

        fp = fopen(inode_file, "r+");
        free(inode_file);

        file_release* f = (file_release*)calloc(1, sizeof(file_release));
        f->fp = fp;
        f->output = NULL;
        fi->fh = (u_int64_t)f;

        pr_deconstruct(p);
        free(p);
    }
    fp = ((file_release*)(fi->fh))->fp;

/* Rewind and read the inode:
 */
    rewind(fp);
	Inode* i = (Inode*)calloc(1, sizeof(Inode));
	inode_read(i, fp);

    unsigned int file_size = i->size;


/* Calculate the new size:
 */
    // ints go up to 2^32 aka 4 million
    // max filesize is 1.6 million
    unsigned int newSize = offset + size;

    // limit the newsize
    if(newSize > SUPERBLOCK->maxFileSize)
    {
        free(i);
        return -EFBIG;
    }

    // writing nothing?
    if(size == 0)
    {
        free(i);
        return file_size;
    }

/* Prep to get the actual raw file:
 */
	char filename[200] = "";
	sprintf(filename, "%s%d", BLOCKPATHHEAD, i->location);
	FILE* fp2 = fopen(filename, "r+");

/* ===== WRITE WILL NEVER REDUCE FILE SIZES =====
 * File size reduction is done with truncate
 * which gets called before write
 */


/* In the case of non-indirect with no conversion to an indirect:
 */
    if(i->indirect != 1 && newSize <= 4096)
    {
        if(FREEBLOCKSCOUNT == 0)
        {
            fclose(fp2);
            free(i);
            return -EFBIG;
        }

    // write directly to the file with an offset
        fseek(fp2, offset, SEEK_SET);
        fwrite(input, 1, size, fp2);

    // update the inode file:
        i->size = newSize;
        // mtimes are updated on write
        i->mtime = time(NULL);
        rewind(fp);
        inode_write(i, fp);
        Inode* in = calloc(1, sizeof(Inode));
        rewind(fp);
        inode_read(in, fp);

    // close the raw file:
        fclose(fp2);

        free(in);
        free(i);
        return size;
    }
/* In the case of a conversion to indirect(file larger than 4096)
 */
    // Set up an index file as both conversion and non-conversion will need it
    int index_file[400];

    // How many blocks
    int oldNumBlocks = (file_size -1)/(SUPERBLOCK->blockSize) +1;
    int totalNumBlocks = (newSize -1)/(SUPERBLOCK->blockSize) +1;
    // there's probably an easier way to do this with % or something but w/e

/* 1. reserve space for filesize + offset + size aka new size
 * 2. move the contents from the raw file to the new block #1
 */

    if(i->indirect != 1 && offset+size > 4096)
    {
    // reserve blocks
        reserve(index_file, 1);

    // grab the old contents from the raw file
        sprintf(filename, "%s%d", BLOCKPATHHEAD, i->location);
        FILE* raw_fp = fopen(filename, "r+");
        char staging_buf[4096] = "";
        fread(staging_buf, 1, 4096, raw_fp);

    // replace contents with an index:
        rewind(raw_fp);
        index_write(index_file, 1, raw_fp);
        fclose(raw_fp);

    //  copy into the new block #1
        sprintf(filename, "%s%d", BLOCKPATHHEAD, index_file[0]);
        FILE* fp3 = fopen(filename, "w+");
        fwrite(staging_buf, 1, 4096, fp3);
        fclose(fp3);

    // now the file is an indirect file
        i->indirect = 1;
    }

/* From this point on the file is an indirect file
 */

/* 1. Create an in-memory buffer to store file
 * 2. Reserve the necessary extra number of blocks
 * 3. Use offset to find the starting block
 * 4. Update starting from that block in increments of SUPERBLOCK->blockSize
 */
    // make enough space to read and write to
    // read/write is done by entire blocks
    // calloc used in the case of partially written to blocks
    // if necessary fread from block first then fwrite to update it
    // since writing is block by block, only need a buffer of 1 block size
    char* buffer = calloc(SUPERBLOCK->blockSize, 1);
    memset(buffer, '\0', SUPERBLOCK->blockSize);

    // reserve new blocks
    int newNumBlocks = totalNumBlocks - oldNumBlocks;
    if(newNumBlocks > FREEBLOCKSCOUNT)
    {
        fclose(fp2);
        free(i);
        return -ENOSPC;
    }

    int blocks[399]; // avoid the heap, use max number of possible requested blocks
    reserve(blocks, newNumBlocks);
    int blocks_index = 0;

    // grab the index
    sprintf(filename, "%s%d", BLOCKPATHHEAD, i->location);
    FILE* index_fp = fopen(filename, "r+");
    index_read(index_file, index_fp);

    // update the index with the newly reserved blocks
    int j;
    for(j = 0; j < totalNumBlocks && blocks_index < newNumBlocks ; j++)
    {
        if(index_file[j] != -1)
        {
            continue;
        }
        index_file[j] = blocks[blocks_index++];
    }

    // write the index
    rewind(index_fp);
    index_write(index_file, totalNumBlocks, index_fp);
    fclose(index_fp);

/* start writing
 */

    /* consider offset = 6000
     * then startingblockindex = 6000/4096 = 1
     * we start writing from position 6000%4096
     * and we write 4096 - (6000%4096) bytes
     */
    FILE* block_file;
    int writeBlockIndex = offset/(SUPERBLOCK->blockSize);
    int initOffset  = offset%(SUPERBLOCK->blockSize);
    int initSize    = (SUPERBLOCK->blockSize) - initOffset;

    int blocksWritten = 0;
    int sizeLeft = size;

    // only the first block will have a partial write:
    // prep the file:
    sprintf(filename, "%s%d", BLOCKPATHHEAD, index_file[writeBlockIndex++]);
    block_file = fopen(filename, "r+");

    // grab from input:
    if(initSize > size) // don't want to risk garbage data
        memcpy(buffer, input, size);
    else
        memcpy(buffer, input, initSize);

    // offset the file cursor:
    fseek(block_file, initOffset, SEEK_SET);

    // write:
    fwrite(buffer, 1, initSize, block_file);
    fclose(block_file);

    // wipe buffer:
    memset(buffer, '\0', SUPERBLOCK->blockSize);
    sizeLeft -= initSize;
    blocksWritten++;

    // for the rest of the blocks
    for(j = writeBlockIndex; j < totalNumBlocks; j++, blocksWritten++)
    {
        sprintf(filename, "%s%d", BLOCKPATHHEAD, index_file[writeBlockIndex++]);
        block_file = fopen(filename, "r+");

        // grab from input:
        memcpy(buffer, input + blocksWritten*SUPERBLOCK->blockSize + initOffset, SUPERBLOCK->blockSize);

        // write:
        fwrite(buffer, 1, SUPERBLOCK->blockSize, block_file);
        fclose(block_file);

        // wipe buffer:
        memset(buffer, '\0', SUPERBLOCK->blockSize);
        sizeLeft -= initSize;
    }

/* DON'T FORGET TO UPDATE THE INODE
*/

    i->size = newSize;
    // mtimes are updated on write
    i->mtime = time(NULL);
    rewind(fp);
    inode_write(i, fp);
    Inode* in = calloc(1, sizeof(Inode));
    rewind(fp);
    inode_read(in, fp);

    fclose(fp2);

    free(in);
    free(i);

    ((file_release*)(fi->fh))->output = buffer;
    return size;

}

/*
static int slam_chmod(const char* path, mode_t mode)
{
    return 100;
}

static int slam_chown(const char* path, uid_t uid, gid_t gid)
{
    return 100;
}

static int slam_utimens(const char* path, const struct timespec t[2])
{
    return t[1].tv_nsec;
}

static int slam_truncate(const char* path, off_t size)
{
    return size;
}
*/


static struct fuse_operations slam_oper =
{
	.create     = slam_create       ,   // k
	.destroy    = slam_destroy      ,   // k
	.getattr    = slam_getattr      ,   // k
	.init       = slam_init         ,   // k
	.link       = slam_link         ,   // k
	.mkdir      = slam_mkdir        ,   // k
	.open       = slam_open         ,   // k
	.opendir    = slam_opendir      ,   // k
	.read       = slam_read         ,   // k
	.readdir    = slam_readdir      ,   // k
	.readlink   = slam_readlink     ,   // k
	.release    = slam_release      ,    // k
	.releasedir = slam_releasedir   ,   // k
	.rename     = slam_rename       ,   // k
//	.chmod      = slam_chmod        ,
//	.chown      = slam_chown        ,
//	.truncate   = slam_truncate     ,
//	.utimens    = slam_utimens      ,
	.statfs     = slam_statfs       ,   // k
	.symlink    = slam_symlink      ,
	.unlink     = slam_unlink       ,   // k
	.write      = slam_write            // k
};

int main(int argc, char *argv[])
{
    char** newargv = calloc(sizeof(char*), argc+2);
    memmove(newargv, argv, sizeof(char*) * argc);

    newargv[argc++] = "-o";
    newargv[argc++] = "use_ino";

    argv = newargv;

	return fuse_main(argc, argv, &slam_oper, NULL);
}
