// ========== MAIN STRUCTS ========== //
static const int MAX_STRING_BUFFER = 200;

typedef struct
{
   unsigned long creationTime;
   int mounted;
   int devId;
   int freeStart;
   int freeEnd;
   int root;
   int maxBlocks;
   unsigned int blockSize;
   unsigned int maxFilename;
   unsigned int maxFileSize;
}Superblock;

typedef struct
{
   int size;
   int uid;
   int gid;
   unsigned long mode;
   unsigned long atime;
   unsigned long ctime;
   unsigned long mtime;
   int linkcount;

}Directory;

typedef struct
{
   char* type;
   char* filename;
   int blocknum;
}Filename_Inode;

typedef struct
{
    unsigned long size;
    int uid; // every file has a uid which refers to the owner of the file, this will be combined with mode permissions to determine access
    int gid; // every file is a gid which refers to the group that is file belongs to this is combined with mode permissions to determine access
    unsigned long mode;
    int linkcount;
    unsigned long atime;
    unsigned long ctime;
    unsigned long mtime;
    int indirect;
    int location;
}Inode;

typedef struct
{
    char* parentName;
    int parentBlock;
    char* childName;
    int childBlock;
    int isFile; // might be useful at some point
    int isLink;
}parseReturn;

typedef struct
{
    FILE* fp;
    char* output;
}file_release;

// ========== END MAIN STRUCTS ========== //

void dir_build(Directory* d,int size, int uid, int gid, unsigned long mode, unsigned long atime, unsigned long crtime, unsigned long mtime, int linkcount);
void fi_build(Filename_Inode* fi, const char* type, const char* filename, int blocknum);
void fi_deconstruct(Filename_Inode* fi);
void pr_deconstruct(parseReturn* p);

void fitostr( Filename_Inode* fi, char* result);
void strtofi(char* s, Filename_Inode* fi);

void dir_write(Directory* d, Filename_Inode* f_array, FILE* fp);
void dir_read(Directory* d, Filename_Inode** f_array, FILE* fp);

void inode_write(Inode* i, FILE* fp);
void inode_read(Inode* i, FILE* fp);

void index_write(int* blocks, int count, FILE* fp);
void index_read(int* blocks, FILE* fp);

//void modeDecode(unsigned long mode)
//{
    // convert mode to string
    // get octal representation strtol
    // octal mod 10, mod 100, mod 1000 for last 3 digits
    // add those three together
    // those are the permissions
    // take the octal representation
    // subtract the permissions
    // convert the octal representation to string again
    // convert the string to hex strtol
    // that is the filetype as defined in stat.h

    // checking permissions can be done with mode & S_PERMISSIONWANTTOCHECK
//}

/*
int isUserOwner(int uid);
int isUserInGroup(int gid);
*/
